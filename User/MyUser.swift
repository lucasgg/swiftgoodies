//
//  MyUser.swift
//  User
//
//  Created by Lucas Guimarães Gonçalves on 19/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

import Foundation

let user = MyUser()

class MyUser: User {
    
    
    override var defaultOptions: [UserDef?] {
        get {
            return [
                UserDef(name: "Map Type", itens: ["Map" : 1, "Satellite" : 2, "Hybrid" : 3], defaultItem: "Hybrid"),
                UserDef(name: "Notifications", itens: ["On" : 1, "Off": 0], defaultItem: "On"),
                UserDef(name: "Name", itens: nil, defaultItem: "Unknown") // Non exaustive option.
            ]
        } }
    

}