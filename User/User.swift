//
//  User.swift
//  User
//
//  Created by Lucas Guimarães Gonçalves on 19/05/15.
//  Copyright (c) 2015 Lucas GG. All rights reserved.
//

///
/// Essa classe gerencia o User Defaults.
/// Para usar você precisa herdar essa classe como no arquivo MyUser.swift.
/// Essa implementação já controla a condição de primeiro salvamento.
///

import Foundation

class User {
    // Constantes para controle, modifique firstTimeSavingKey se quiser.
    static let firstTimeSavingKey = "02232102-0E18-11E5-81CF-3C54B6C90E9A"
    static let standardUserDefaults = NSUserDefaults.standardUserDefaults()
    
    /* Defaults */
    struct UserDef {
        let name: String
        let itens: [String: Int]? // Use nil para que opção seja não exaustiva.
        let defaultItem: String
        var selectedItem: String
        
        init?(name: String, itens: [String: Int]?, defaultItem: String) {
            if !UserDef.userDefIsValid(name, itens: itens, defaultItem: defaultItem) {
                return nil
            }
            
            self.name = name
            self.itens = itens
            self.defaultItem = defaultItem
            self.selectedItem = defaultItem
        }
        
        static func userDefIsValid(name: String, itens: [String: Int]?, defaultItem: String) -> Bool {
            // Nome não pode ser vazio.
            if name == "" {
                return false
            }
            
            // Nome não pode ser igual a chave que controla a condição de salvamento.
            if name == firstTimeSavingKey {
                return false
            }
            
            // O item default deve existir nos itens.
            if itens != nil {
                if itens![defaultItem] == nil {
                    return false
                }
            }
            
            return true
        }
    }
    
    // Opções.
    var options: [UserDef] = []
    var defaultOptions: [UserDef?] { get { return [] } }
    
    
    init() {
        for userDef in defaultOptions {
            if userDef != nil {
                options.append(userDef!)
            }
        }
    }
    
    private func setValuesToUserDefaults() {
        // Coloca as opções no default.
        for userDef in options {
            User.standardUserDefaults.setObject(userDef.selectedItem, forKey: userDef.name)
            
            println("Saved item: \(userDef.selectedItem) for option \(userDef.name)") // DEBUG
        }
    }
    
    final func save() {
        // Verifica se é a primeira vez que está salvando.
        if let firstSavingTime: AnyObject =
            User.standardUserDefaults.objectForKey(User.firstTimeSavingKey) {
                // Salva nos defaults.
                setValuesToUserDefaults()
                // Sincroniza os defaults.
                User.standardUserDefaults.synchronize();
        } else {
            resetUserDefaults()
        }
    }
    
    final func load() {
        // Verify if is the first time the program is loading.
        if let firstSavingTime: AnyObject =
            User.standardUserDefaults.objectForKey(User.firstTimeSavingKey) {
                // Load options.
                for (indexOfUserDef, userDef) in enumerate(options) {
                    // Read the values that can be read.
                    if let selectedItemForUserDef =
                        User.standardUserDefaults.objectForKey(userDef.name) as? String {
                            // Set correct value on correct index.
                            options[indexOfUserDef].selectedItem = selectedItemForUserDef
                    } else {
                        // Reset this information.
                        options[indexOfUserDef].selectedItem = userDef.defaultItem
                    }
                    
                    println("Selected item: \(options[indexOfUserDef].selectedItem) for option \(userDef.name)") // DEBUG
                }
        } else {
            resetUserDefaults()
        }
    }
    
    private func resetUserDefaults() {
        var defaultDictionary: [String : String] = [User.firstTimeSavingKey : ""]
        
        for userDef in defaultOptions {
            if userDef != nil {
                defaultDictionary.updateValue(userDef!.defaultItem, forKey: userDef!.name)
            }
        }
        
        // Register and synchronize defaults.
        User.standardUserDefaults.registerDefaults(defaultDictionary)
        User.standardUserDefaults.synchronize()
        
        load()
    }
    
    /** Return the selected item for certain user default.
    */
    final func getItemOfUserDefByName(name: String) -> (selectedItem: String, selectedItemValue: Int)? {
        for userDef in options {
            if userDef.name == name {
                if let itens = userDef.itens {
                    return (userDef.selectedItem, itens[userDef.selectedItem]!)
                } else {
                    return (userDef.selectedItem, 0)
                }
            }
        }
        
        return nil
    }
    
    /** Select item on user default by item value.
    * Return false if this UserDef is non-exaustive or dosnt exist or item value dosnt exist.
    */
    final func selectItemForUserDefByValue(itemValue: Int, userDefName: String) -> Bool {
        for (indexOfUserDef, userDef) in enumerate(options) {
            if userDef.name == userDefName {
                if let itens = userDef.itens {
                    for value in itens {
                        if value.1 == itemValue {
                            options[indexOfUserDef].selectedItem = value.0
                            return true
                        }
                    }
                }
                
                break
            }
        }
        
        return false
    }
    
    /** Select item on user default by item name.
    * Return false if this UserDef is non-exaustive or dosnt exist or item name dosnt exist.
    */
    final func selectItemForUserDefByName(itemName: String, userDefName: String) -> Bool {
        for (indexOfUserDef, userDef) in enumerate(options) {
            if userDef.name == userDefName {
                if let itens = userDef.itens {
                    if let value = itens[itemName] {
                        options[indexOfUserDef].selectedItem = itemName
                        return true
                    }
                }
                
                break
            }
        }
        
        return false
    }
    
    /** Set selected item on non exaustive user default.
    * Return false if this UserDef is exaustive or dosnt exist.
    */
    final func setSelectedItemForNonExaustUserDef(selectedItem: String, userDefName: String) -> Bool {
        for (indexOfUserDef, userDef) in enumerate(options) {
            if userDef.name == userDefName {
                if userDef.itens == nil {
                    options[indexOfUserDef].selectedItem = selectedItem
                    return true
                }
                
                break
            }
        }
        
        return false
    }
    
}